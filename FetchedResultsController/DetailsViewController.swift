//
//  DetailsViewController.swift
//  FetchedResultsController
//
//  Created by Jahnavi on 11/11/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import AVKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var item = Items()
    
    class func viewController(item: Items!) -> DetailsViewController {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController =  mainStoryboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        viewController.item = item
        return viewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameLabel.text = item.channelTitle
        self.descriptionLabel.text = item.snippetDescription
        dateAndTimeLabel.text =  getDate()
        videoImageView.sd_setImage(with: NSURL(string: (item.thumbnailsHighURL)!)! as URL , placeholderImage: nil, options: SDWebImageOptions.continueInBackground, completed: nil)
    }
    
    
    func getDate() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: item.snippetPublishedAt!)
        dateFormatter.dateFormat = "MMM dd,YYYY HH:mm a"
        print("Dateobj: \(dateFormatter.string(from: date!))")
        return dateFormatter.string(from: date!)
    }
    
    @IBAction func videoImageButtonTapped(_ sender: UIButton) {
        guard (currentReachabilityStatus != .notReachable) == true else {
            presentAlertViewWithTitle("", message: "Please check your internet connectivity")
            return
        }
        loadVideo()
    }
    
    func presentAlertViewWithTitle(_ title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(alertAction)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let currentViewController = appDelegate?.window?.rootViewController {
            currentViewController.present(alertController, animated: true, completion: nil)
        }
    }
    
    func loadVideo() {
        var youtubeUrl = NSURL(string:"youtube://\(item.videoId!)")!
        if UIApplication.shared.canOpenURL(youtubeUrl as URL){
            UIApplication.shared.open(youtubeUrl as URL, options: [:], completionHandler: nil)
        } else{
            youtubeUrl = NSURL(string:"https://www.youtube.com/watch?v=\(item.videoId!)")!
            UIApplication.shared.open(youtubeUrl as URL, options: [:], completionHandler: nil)
        }
    }
    
    
}
