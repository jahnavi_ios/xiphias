//
//  ServiceHandler.swift
//  FetchedResultsController
//
//  Created by Jahnavi on 11/11/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreData

class ServiceHandler: NSObject {
    
    let manageObjectContextForItems = ManagedObjectClass.managedObjectContext(isForBackgroundThread: false, mergeDataChangesFromOtherContexts: true)
    
    var arrayOfHighResoulutionImagesStringArray = [String]()
    
    func getList( successHandler: @escaping (_ successMessage: String) -> Void, errorHandler: (_ errorMessage: String) -> Void, completionHandler: () -> Void) {
        
        let headers: HTTPHeaders = [
            "Host": "www.googleapis.com",
            ]
        
        Alamofire.request("https://www.googleapis.com/youtube/v3/search?part=id,snippet&maxResults=20&channelId=UCCq1xDJMBRF61kiOgU90_kw&key=AIzaSyBRLPDbLkFnmUv13B-Hq9rmf0y7q8HOaVs", headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                Items.purge()
                let arrayOfList = JSON(response.result.value!)["items"].array
                guard arrayOfList?.count != 0 else {
                    successHandler("no items available")
                    return
                }
                for aItem in arrayOfList! {
                    let itemsInCoreData: Items = Items.managedObject(self.manageObjectContextForItems)
                    itemsInCoreData.kind = aItem["kind"].stringValue
                    itemsInCoreData.kindid = aItem["id"]["kind"].stringValue
                    itemsInCoreData.videoId = aItem["id"]["videoId"].stringValue
                    itemsInCoreData.etag = aItem["etag"].stringValue
                   // itemsInCoreData.channelTitle = aItem["snippet"]["title"].stringValue
                    itemsInCoreData.snippetPublishedAt = aItem["snippet"]["publishedAt"].stringValue
                    itemsInCoreData.snippetDescription =  aItem["snippet"]["description"].stringValue
                    itemsInCoreData.thumbNailsDefaultURL =  aItem["snippet"]["thumbnails"]["default"]["url"].stringValue
                    itemsInCoreData.thumnailsDefaultWidth = "\(aItem["snippet"]["thumbnails"]["default"]["width"].intValue)"
                    itemsInCoreData.thumbnailsDefaultHeight = "\(aItem["snippet"]["thumbnails"]["default"]["height"].intValue)"
                    itemsInCoreData.thumnailsMediumURL =  aItem["snippet"]["thumbnails"]["medium"]["url"].stringValue
                    itemsInCoreData.thumnailsMediumWidth = "\(aItem["snippet"]["thumbnails"]["medium"]["width"].intValue)"
                    itemsInCoreData.thumbnailsMediumHeight = "\(aItem["snippet"]["thumbnails"]["medium"]["height"].intValue)"
                    itemsInCoreData.thumbnailsHighURL =  aItem["snippet"]["thumbnails"]["high"]["url"].stringValue
                    itemsInCoreData.thumbnailsHighWidth = "\(aItem["snippet"]["thumbnails"]["high"]["width"].intValue)"
                    itemsInCoreData.thumbnailsHighHeight = "\(aItem["snippet"]["thumbnails"]["high"]["height"].intValue)"
                    itemsInCoreData.channelTitle = aItem["snippet"]["channelTitle"].stringValue
                }
                try! self.manageObjectContextForItems.save()
                print(Items.getManagedObjects(self.manageObjectContextForItems).count)
                successHandler("Success")
            case .failure:
                print("error")
            }
        }
    }

}
