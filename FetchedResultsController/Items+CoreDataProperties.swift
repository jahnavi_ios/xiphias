//
//  Items+CoreDataProperties.swift
//  FetchedResultsController
//
//  Created by Jahnavi on 11/11/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//
//

import Foundation
import CoreData


extension Items {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Items> {
        return NSFetchRequest<Items>(entityName: "Items")
    }

    @NSManaged public var kind: String?
    @NSManaged public var etag: String?
    @NSManaged public var kindid: String?
    @NSManaged public var videoId: String?
    @NSManaged public var snippetPublishedAt: String?
    @NSManaged public var snippetChannelID: String?
    @NSManaged public var snippetTitle: String?
    @NSManaged public var snippetDescription: String?
    @NSManaged public var thumbNailsDefaultURL: String?
    @NSManaged public var thumnailsDefaultWidth: String?
    @NSManaged public var thumbnailsDefaultHeight: String?
    @NSManaged public var thumnailsMediumURL: String?
    @NSManaged public var thumnailsMediumWidth: String?
    @NSManaged public var thumbnailsMediumHeight: String?
    @NSManaged public var thumbnailsHighURL: String?
    @NSManaged public var thumbnailsHighWidth: String?
    @NSManaged public var thumbnailsHighHeight: String?
    @NSManaged public var channelTitle: String?
    @NSManaged public var liveBroadcastContent: String?

}
