//
//  ListTableViewController.swift
//  FetchedResultsController
//
//  Created by Jahnavi on 11/11/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage

class ListTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
     var itemsFetchResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    var itemsManagedObjectContext = ManagedObjectClass.managedObjectContext(isForBackgroundThread: false, mergeDataChangesFromOtherContexts: true)

    
      var fetchingItems = false

    class func viewController() -> ListTableViewController {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        return mainStoryboard.instantiateViewController(withIdentifier: "ListTableViewController") as! ListTableViewController
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.itemsFetchResultsController = Items.fetchedResultsController(self.itemsManagedObjectContext, sortKeys: ["channelTitle"], isAscending: false)
        itemsFetchResultsController.delegate = self
        try! itemsFetchResultsController.performFetch()
        self.tableView.reloadData()
        guard (currentReachabilityStatus != .notReachable) == true else {
            if itemsFetchResultsController.fetchedObjects?.count == 0 {
                presentAlertViewWithTitle("", message: "Please check your internet connectivity")
            }
            return
        }
        ServiceHandler().getList(successHandler: { (successMessage) in
            if successMessage == "Success" {
                    self.fetchingItems = false
                    self.itemsFetchResultsController = Items.fetchedResultsController(self.itemsManagedObjectContext, sortKeys: ["channelTitle"], isAscending: false)
                    try! self.itemsFetchResultsController.performFetch()
                    self.tableView.reloadData()
            } else {
            }
        }, errorHandler: { (error) in
                self.fetchingItems = false
               print("failure")
        }) {
            //
        }
    }
    
    func presentAlertViewWithTitle(_ title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        alertController.addAction(alertAction)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let currentViewController = appDelegate?.window?.rootViewController {
            currentViewController.present(alertController, animated: true, completion: nil)
        }
    }
    
    
   

    // MARK: TableView Data source and Delegate methods
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsFetchResultsController.fetchedObjects?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as! ListTableViewCell
        let items = itemsFetchResultsController.object(at: indexPath) as? Items
        cell.configureCell(item: items!)
        cell.videoImageView.sd_setImage(with: NSURL(string: (items?.thumbnailsHighURL)!)! as URL , placeholderImage: nil, options: SDWebImageOptions.continueInBackground, completed: nil)
        return cell
    }
    
     override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.pushViewController(DetailsViewController.viewController(item: itemsFetchResultsController.object(at: indexPath) as? Items), animated: true)
        
    }
    
    

    
}
