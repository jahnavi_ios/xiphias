//
//  ManagedObjectClass.swift
//  FetchedResultsController
//
//  Created by Jahnavi on 11/11/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit
import CoreData

class ManagedObjectClass:NSManagedObjectContext {
    
    // MARK: Factory Methods
    class func managedObjectContext(isForBackgroundThread: Bool = false, mergeDataChangesFromOtherContexts: Bool = false) -> ManagedObjectClass {
        let concurrencyType: NSManagedObjectContextConcurrencyType = isForBackgroundThread ? .privateQueueConcurrencyType : .mainQueueConcurrencyType
        let managedObjectContext = ManagedObjectClass(concurrencyType: concurrencyType)
        managedObjectContext.persistentStoreCoordinator = ManagedObjectClass.sharedPersistentStoreCoordinator
        
        if mergeDataChangesFromOtherContexts {
            NotificationCenter.default.addObserver(managedObjectContext, selector: #selector(mergeDataChanges(_:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
        }
        return managedObjectContext
    }
    
   
    private static var sharedPersistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let aPersistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: ManagedObjectClass.sharedManagedObjectModel)
        let dataMigrationOptions = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]   // Enable automatic migration of the database to a new version.
        try! aPersistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: ManagedObjectClass.SQLiteFileURL, options: dataMigrationOptions)
        return aPersistentStoreCoordinator;
    }()
    
    private static var sharedManagedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "FetchedResultsController", withExtension: "momd")
        return NSManagedObjectModel(contentsOf: modelURL!)!
    }()
    
    private static var SQLiteFileURL: URL = {
        if let applicationDocumentsDirectoryURL = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).last {
            let SQLiteFileName: String = "FetchedResultsController"
            return applicationDocumentsDirectoryURL.appendingPathComponent(SQLiteFileName).appendingPathExtension("sqlite")
        } else {
            let applicationSupportDirectoryPaths = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true)
            let SQLiteDirectory = applicationSupportDirectoryPaths.first! + "/SQLite"
            if !FileManager.default.fileExists(atPath: SQLiteDirectory) {
                try! FileManager.default.createDirectory(atPath: SQLiteDirectory, withIntermediateDirectories: true, attributes: nil)
            }
            let SQLiteFileName:String = "FetchedResultsController"
            return URL(fileURLWithPath: SQLiteDirectory, isDirectory: true).appendingPathComponent(SQLiteFileName).appendingPathExtension("sqlite")
        }
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(concurrencyType ct: NSManagedObjectContextConcurrencyType) {
        super.init(concurrencyType: ct)
    }
    
    @objc func mergeDataChanges(_ notificationsChangeNotification: Foundation.Notification) {
        let notifier = notificationsChangeNotification.object as! NSManagedObjectContext
        if notifier.persistentStoreCoordinator !== ManagedObjectClass.sharedPersistentStoreCoordinator { return }
        
        if notifier === self { return }
        perform { self.mergeChanges(fromContextDidSave: notificationsChangeNotification) }
    }
    
}

