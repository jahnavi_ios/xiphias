//
//  ActivityIndicatorHelper.swift
//  FetchedResultsController
//
//  Created by Jahnavi on 11/11/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit

class ActivityIndicatorHelper: NSObject {
    
    fileprivate let activityIndicator:          UIActivityIndicatorView!
    fileprivate var taggedViewConttoller:       UIViewController!
    fileprivate let viewForActivityIndicator  = UIView()
    fileprivate var isFromTableView           = false
    fileprivate var isUserInteractionDisabled = false
    fileprivate var isOnMainQueue             = false
    fileprivate var backgroundColor: UIColor!
    
    required init(viewController:UIViewController, style: UIActivityIndicatorViewStyle, isUserInteractionDisabled: Bool, onMainQueue: Bool, backgroundColor:UIColor) {
        isOnMainQueue = onMainQueue
        self.isUserInteractionDisabled = isUserInteractionDisabled
        self.taggedViewConttoller      = viewController
        self.activityIndicator         = UIActivityIndicatorView(activityIndicatorStyle: style)
        self.isFromTableView           = viewController.isKind(of: UITableViewController.self)
        self.backgroundColor           = backgroundColor
    }
    
    deinit { viewForActivityIndicator.removeFromSuperview() }
    
    func setupActivityIndicator() {
        if isOnMainQueue == true {
            DispatchQueue.main.async { self.setupActivityIndicatorLocal() }
        } else { setupActivityIndicatorLocal() }
    }
    
    fileprivate func setupActivityIndicatorLocal() {
        viewForActivityIndicator.frame  = taggedViewConttoller.view.frame
        viewForActivityIndicator.addSubview(activityIndicator)
        viewForActivityIndicator.backgroundColor = backgroundColor
        viewForActivityIndicator.alpha  = 0.5
        activityIndicator.center        = viewForActivityIndicator.center
        viewForActivityIndicator.isHidden = true
        taggedViewConttoller.view.addSubview(viewForActivityIndicator)
    }
    
    func startActivityIndicator() {
        if isOnMainQueue == true {
            DispatchQueue.main.async { self.startActivityIndicatorLocal() }
        } else { startActivityIndicatorLocal() }
    }
    
    fileprivate func startActivityIndicatorLocal() {
        activityIndicator.startAnimating()
        viewForActivityIndicator.isHidden = false
        viewForActivityIndicator.frame  =  CGRect(x: 0, y: 0, width: taggedViewConttoller.view.frame.width, height: taggedViewConttoller.view.frame.height)
        if isFromTableView == true {
            let tableViewContorller = taggedViewConttoller as! UITableViewController
            viewForActivityIndicator.frame = CGRect(x: tableViewContorller.tableView.contentOffset.x, y: tableViewContorller.tableView.contentOffset.y, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            if isUserInteractionDisabled == true { tableViewContorller.tableView.isUserInteractionEnabled = false }
        }
        viewForActivityIndicator.bringSubview(toFront: activityIndicator)
        if isUserInteractionDisabled == true { taggedViewConttoller?.view.isUserInteractionEnabled = false }
    }
    
    fileprivate func stopActivityIndicatorLocal() {
        if isUserInteractionDisabled == true { taggedViewConttoller.view.isUserInteractionEnabled = true }
        viewForActivityIndicator.isHidden     = true
        activityIndicator.stopAnimating()
        if isFromTableView == true && isUserInteractionDisabled == true{
            let tableViewContorller = taggedViewConttoller as! UITableViewController
            tableViewContorller.tableView.isUserInteractionEnabled = true
        }
    }
    
    func stopActivityIndicator() {
        if isOnMainQueue == true {
            DispatchQueue.main.async { self.stopActivityIndicatorLocal() }
        } else { stopActivityIndicatorLocal() }
    }
    
    func setColor(_ color:UIColor) { activityIndicator.color = color }
    
}



