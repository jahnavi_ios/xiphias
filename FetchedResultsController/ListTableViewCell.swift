//
//  ListTableViewCell.swift
//  FetchedResultsController
//
//  Created by Jahnavi on 11/11/17.
//  Copyright © 2017 Jahnavi. All rights reserved.
//

import UIKit
import AVKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var hastTagLabel: UILabel!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var item : Items!
    
    func configureCell(item: Items) {
        self.item = item
        self.nameLabel.text = item.channelTitle
        dateAndTimeLabel.text = getDate()
        self.descriptionLabel.text = item.snippetDescription
    }
    
    
    func getDate() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: item.snippetPublishedAt!)
        dateFormatter.dateFormat = "MMM dd,YYYY HH:mm a"
        print("Dateobj: \(dateFormatter.string(from: date!))")
        return dateFormatter.string(from: date!)
    }
  
    func openVideo() {
       
    }
    
    @IBAction func VideoImageButtonTapped(_ sender: UIButton) {
        openVideo()
    }
    
    
  
    
}
